/*
    https://deckofcardsapi.com/
    Bataille bretonne : 2 cartes par 2 cartes
    1- Créer un deck = Brand New Deck
    2- Créer deux piles = Adding to pile
    3- Afficher 2 carte de chaque pile = Drawing from Piles (?count=2)
    4- Comparer les quatre cartes = Fonction interne
    5- Rajouter les 4 cartes dans la pile du joueurs gagnant = Adding to Piles
    6- Arrêter la partie quand une pile est vide
 */

let players = [
    {
        name : 'Toto',
        pile : 'pile1',
        card_played : ''
    },
    {
        name : 'Jean claude',
        pile : 'pile2',
        card_played : ''
    }
]

let deck;

$(document).ready(function () {
    nouvellePartie();

    $('#commencer').click(function () {
        $(this).hide();
        jouerCartesPile();

        $('#suivant').show();

    });

    $('#j1').text(players[0].name);
    $('#j2').text(players[1].name);

    $('#suivant').click( function () {
        $('.poper').hide();

        jouerCartesPile();

        $('#carte-0').hide();
        $('#carte-1').hide();
    })

})

function nouvellePartie () {
    $.ajax({
        url: 'https://deckofcardsapi.com/api/deck/new/shuffle',
        type: 'GET',
        contentType: 'application/json',
        success: function (data) {
            deck = data.deck_id;
            distributionCartes()
        },

        error: function (data) {
        },

        complete: function (resultat, statut) {

        }
    })
}

function distributionCartes () {
    for (let num_player in players) {
        $.ajax({
            url: 'https://deckofcardsapi.com/api/deck/'+deck+'/draw/?count=26',
            type: 'GET',
            contentType: 'application/json',
            success: function (data) {
                let cards = data.cards.map(function(elem){return elem.code;}).join(",");
                ajoutCartesPile(num_player,cards)
            },

            error: function (data) {
            },

            complete: function (resultat, statut) {

                if(num_player == players.length-1) {
                    $('#commencer').show();
                }
            }
        });
    };

}

function ajoutCartesPile (num_player,cards) {
    $.ajax({
        url: 'https://deckofcardsapi.com/api/deck/'+deck+'/pile/'+players[num_player].pile+'/add/?cards='+cards,
        type: 'GET',
        contentType: 'application/json',
        success: function (data) {
            if (data.piles.pile1 && data.piles.pile2) {
                $('#nb-cartes-0').text(data.piles.pile1.remaining);
                $('#nb-cartes-1').text(data.piles.pile2.remaining);
                if(data.piles.pile1.remaining === 0) {
                    $('#fin-partie').css('display','flex');
                    $('#gagnant').text(players[1].name+' a gagné !')
                } else if(data.piles.pile2.remaining === 0) {
                    $('#fin-partie').css('display','flex');
                    $('#gagnant').text(players[0].name+' a gagné !')
                }
            }
        },

        error: function (data) {
        },

        complete: function (resultat, statut) {

        }
    });
}

async function jouerCartesPile () {
    for (let num_player in players) {
        $.ajax({
            url: 'https://deckofcardsapi.com/api/deck/' + deck + '/pile/' + players[num_player].pile + '/draw/bottom',
            type: 'GET',
            contentType: 'application/json',
            success: function (data) {
                $('#carte-' + num_player).attr('src', data.cards[0].image);
                $('#carte-' + num_player).show();
                players[num_player].card_played = data.cards[0].code;
            },

            error: function (data) {
            },

            complete: function (resultat, statut) {
                if(num_player == players.length-1) {
                    setTimeout(function () {
                        comparerCarte();
                    }, 500);
                }
            }
        });
    }
}

function comparerCarte () {

    if (valeurCarte(players[0].card_played) >valeurCarte(players[1].card_played)) {
        ajoutCartesPile(0,players[0].card_played+','+players[1].card_played);
        $('#j1-plus').show();
        $('#j2-moins').show();

    } else if (valeurCarte(players[0].card_played) <valeurCarte(players[1].card_played)){
        ajoutCartesPile(1,players[0].card_played+','+players[1].card_played);
        $('#j1-moins').show();
        $('#j2-plus').show();
    }

}

function valeurCarte (code) {
    let valeur;
    if (code.charAt(0) > 1 && code.charAt(0) < 10 ) {
        valeur = code.charAt(0)
    } else if (code.charAt(0) === '0') {
        valeur = 10;
    } else if (code.charAt(0) === 'J') {
        valeur = 11;
    } else if (code.charAt(0) === 'Q') {
        valeur = 12;
    } else if (code.charAt(0) === 'K') {
        valeur = 13;
    } else if (code.charAt(0) === 'A') {
        valeur = 14;
    }

    return valeur;
}